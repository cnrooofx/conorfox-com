import * as React from 'react'
import { StaticImage } from 'gatsby-plugin-image'
import Layout from '../components/layout'

import "../styles/index.css"

const IndexPage = () => {
  return (
    <Layout>
      <figure>
        <StaticImage
          src="../images/bologna.avif"
          alt="Parco Del San Pellegrino, Bologna"
          loading="eager"
          className="image"
        />
        <figcaption>Parco Del San Pellegrino, Bologna</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/sanluca.avif"
          alt="Santuario della Madonna di San Luca, Bologna"
          loading="eager"
          className="image"
        />
        <figcaption>Santuario della Madonna di San Luca, Bologna</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/firenze.avif"
          alt="Piazza del Duomo, Firenze"
          className="image"
        />
        <figcaption>Piazza del Duomo, Firenze</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/riomaggiore.avif"
          alt="Parco Nazionale delle Cinque Terre, Riomaggiore"
          className="image"
        />
        <figcaption>Parco Nazionale delle Cinque Terre, Riomaggiore</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/triomphe.avif"
          alt="Arc de triomphe de l'Étoile, Paris"
          className="image"
        />
        <figcaption>Arc de triomphe de l'Étoile, Paris</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/eiffel.avif"
          alt="Tour Eiffel, Paris"
          className="image"
        />
        <figcaption>Tour Eiffel, Paris</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/chantilly.avif"
          alt="Chateau de Chantilly"
          className="image"
        />
        <figcaption>Ch&acirc;teau de Chantilly</figcaption>
      </figure>
      <figure>
        <StaticImage
          src="../images/heidelberg.avif"
          alt="Heidelberger Schloss, Heidelberg"
          className="image"
        />
        <figcaption>Heidelberger Schloss, Heidelberg</figcaption>
      </figure>
    </Layout>

  )
}

export const Head = () => (
  <>
    <title>Conor</title>
    <link rel="preload" href="/fonts/Inter-roman.var.woff2" as="font" type="font/woff2" crossOrigin="anonymous" />
  </>
)

export default IndexPage
