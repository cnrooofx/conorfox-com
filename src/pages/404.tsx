import * as React from "react"
import { Link, HeadFC, PageProps } from "gatsby"

import "../styles/index.css"

const pageStyles: React.CSSProperties = {
  margin: "5em auto",
}

const textStyles: React.CSSProperties = {
  textAlign: "center",
  margin: "0.5em",
}

const NotFoundPage: React.FC<PageProps> = () => {
  return (
    <main style={pageStyles}>
      <h1 style={textStyles}>Page not found</h1>
      <p style={textStyles}>
        Sorry, we couldn't find the page you were looking for.
      </p>
      <p style={textStyles}>
        <Link to="/">Go home</Link>
      </p>
    </main>
  )
}

export default NotFoundPage

export const Head: HeadFC = () => <title>Not found</title>
