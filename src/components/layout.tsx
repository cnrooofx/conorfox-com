import * as React from 'react'

const Layout = ({ children }) => {
  return (
    <>
      <header>
        <h1>[ CONOR ]</h1>
      </header>
      <main>
        { children }
      </main>
      <footer>
        <small>&copy; Conor Fox 2023</small>
      </footer>
    </>
  )
}

export default Layout
